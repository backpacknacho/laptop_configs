#!/usr/bin/bash

if [ $(whoami) != "nacho" ]; then
  echo "This script needs to be run as nacho"
  exit 1
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

mkdir -p ~/.config/i3/
cp $SCRIPT_DIR/config_files/h-dotconfig-i3-config ~/.config/i3/config
cp $SCRIPT_DIR/config_files/h-dotxinitrc ~/.xinitrc
cp $SCRIPT_DIR/config_files/h-dotXresources ~/.Xresources
cp $SCRIPT_DIR/config_files/h-dotssh-config ~/.ssh/config

echo "Finish message"
