#!/usr/bin/bash

if [ $(whoami) != "root" ]; then
  echo "This script needs to be run as root"
  exit 1
fi

if [ ! -e /home/nacho ]; then
	echo "Executing first install directives";
	sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist
	useradd -m nacho
	usermod -aG wheel,video nacho
	hostnamectl set-hostname inception
fi

systemctl enable iwd
systemctl enable dhcpcd

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cp $SCRIPT_DIR/config_files/etc-pacman.conf /etc/pacman.conf
cp $SCRIPT_DIR/config_files/etc-systemd-logind.conf /etc/systemd/logind.conf
cp $SCRIPT_DIR/config_files/etc-hosts /etc/hosts

cp $SCRIPT_DIR/services/laptop-boot.service /etc/systemd/system/
systemctl enable laptop-boot.service
timedatectl set-ntp true

echo "Finish message"
